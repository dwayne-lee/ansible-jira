# ansible-jira
# Use Ansible to install a Jira server with all required dependencies

### Prepare your environment
1. Edit the hosts file in the current dir, adding your target hosts(s).
2. Download the PEM file for the target host(s) to your $HOME/.ssh folder.  (Make sure perms are 0400)
3. Run `ssh-add ~/.ssh/key-pair-name.pem`

### Run the Ansible playbook
1. Run `ansible-playbook -i hosts playbook.yml -D --check`  ....to dry-run in check mode.
2. Run `ansible-playbook -i hosts playbook.yml -D`  ....for the real deal.

Please contact Dwayne Lee (Dwayne.Lee@Ellucian.com) with any issues, suggestions.  Enjoy!
